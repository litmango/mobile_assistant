package com.kylin.mobileassistant.data;

import android.net.Uri;

public class PictureInfo extends BaseInfo {
    private final Uri thumbUri;

    public PictureInfo(String path, String name, Uri uri, long size) {
        super(path, name, size);
        this.thumbUri = uri;
    }

    public Uri getUri() {
        return thumbUri;
    }
}
