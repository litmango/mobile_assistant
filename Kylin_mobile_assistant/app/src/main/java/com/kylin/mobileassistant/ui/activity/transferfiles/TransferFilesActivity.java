package com.kylin.mobileassistant.ui.activity.transferfiles;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.base.view.BaseMvpActivity;
import com.kylin.mobileassistant.presenter.transferfiles.TransferFilesActivityPresenter;
import com.kylin.mobileassistant.ui.activity.interfaces.transferfiles.ITransferFilesActivity;

public class TransferFilesActivity extends BaseMvpActivity<TransferFilesActivityPresenter>
        implements ITransferFilesActivity {
    private ViewPager2 viewPager2 = null;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_transfer_files;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_files);
        mPresenter.loadIcons(getWindow().getDecorView());
        ViewPager2 viewPager = findViewById(R.id.transfer_files_view_pager);
        viewPager2 = viewPager;
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        final ApplicationsFragment applicationsFragment = new ApplicationsFragment();
        final PicturesFragment picturesFragment = new PicturesFragment();
        final DocumentsFragment documentsFragment = new DocumentsFragment();
        final VideosFragment videosFragment = new VideosFragment();
        final MusicsFragment musicsFragment = new MusicsFragment();
        viewPager.setAdapter(new FragmentStateAdapter(this) {
            @NonNull
            @Override
            public Fragment createFragment(int position) {
                switch (position) {
                    case 0:
                        return applicationsFragment;
                    case 1:
                        return picturesFragment;
                    case 2:
                        return documentsFragment;
                    case 3:
                        return videosFragment;
                    case 4:
                        return musicsFragment;
                }
                return applicationsFragment;
            }

            @Override
            public int getItemCount() {
                return 5;
            }
        });
        new TabLayoutMediator(tabLayout, viewPager, true, (tab, position) -> {
            switch (position) {
                case 0:
                    tab.setText("应用");
                    break;
                case 1:
                    tab.setText("图片");
                    break;
                case 2:
                    tab.setText("文档");
                    break;
                case 3:
                    tab.setText("视频");
                    break;
                case 4:
                    tab.setText("音乐");
            }
        }).attach();
    }

    @Override
    protected void initPresenter() {
        mPresenter = new TransferFilesActivityPresenter();
    }

    public void setViewPagerSlide(boolean b) {
        viewPager2.setUserInputEnabled(b);
    }
}
