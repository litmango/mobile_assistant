package com.kylin.mobileassistant.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.TypedValue;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static android.content.Context.ACTIVITY_SERVICE;

public class AppUtils {

    //申请读写权限
    public static void verifyStoragePermissions(Activity activity) {
        //权限
        final int REQUEST_EXTERNAL_STORAGE = 1;
        final String[] PERMISSIONS_STORAGE = {
                Manifest.permission.READ_SMS,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
        };
        try {
            //检测是否有写的权限
            int permission = ActivityCompat.checkSelfPermission(activity,
                    "android.permission.WRITE_EXTERNAL_STORAGE");
            if (permission != PackageManager.PERMISSION_GRANTED) {
                // 没有写的权限，去申请写的权限，会弹出对话框
                ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE,REQUEST_EXTERNAL_STORAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R &&
                !Environment.isExternalStorageManager()) {

            Intent intent = new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);
            System.out.println("ok");
        }
    }

    /**
     * 判断是否有权限
     */
    public static boolean appHasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 判断是否有权限 shouldShowRequestPermissionRationale.（点了不再提示）
     */
    public static boolean shouldShowRequestPermissionRationale(Fragment fragment, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && fragment != null && permissions != null) {
            for (String permission : permissions) {
                if (fragment.shouldShowRequestPermissionRationale(permission)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 网络是否可用
     */
    public static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * 获取imei，获取不到使用android id
     *
     */
    @SuppressLint("MissingPermission")
    public static String getDeviceId(Context context) {

        if (!appHasPermissions(context, Manifest.permission.READ_PHONE_STATE)) {
            // device Id must not be null
            return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }

        String identifier = null;
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (tm != null) {
            identifier = tm.getDeviceId();
        }
        if (!isCorrectDeviceId(identifier)) {
            identifier = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }

        return identifier;
    }

    /**
     * deviceId是否正确
     * @param deviceId
     * @return
     */
    private static boolean isCorrectDeviceId(String deviceId) {
        if (deviceId == null || deviceId.length() == 0) {
            return false;
        }

        return !deviceId.matches("[0]+");
    }

    /**
     * 设置的intent
     * @param context
     * @return
     */
    public static Intent getSettingsIntent(Context context) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.fromParts("package", context.getPackageName(), null));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    /**
     * action view的intent
     * @param url
     * @return
     */
    @NonNull
    public static Intent getViewIntent(String url) {
        return new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    }

    /**
     * 安装的intent
     * @param context
     * @param file
     * @return
     */
    public static Intent getUpdateIntent(Context context, File file) {

        if (file != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Uri apkUri = FileProvider.getUriForFile(context, context.getApplicationInfo().packageName + ".provider", file);
                Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                intent.setData(apkUri);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK);
                return intent;
            } else {
                Uri apkUri = Uri.fromFile(file);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                return intent;
            }
        }
        return null;
    }


    /**
     * 获取设备名字
     */
    @NonNull
    public static String getDeviceName() {
        return Build.MANUFACTURER + " " + Build.MODEL;
    }

    /**
     * @return versionName
     */
    public static String getVersionName(Context context) {
        PackageManager packageManager = context.getPackageManager();
        // 0 is get version
        try {
            PackageInfo packInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            if (packInfo != null) {
                return packInfo.versionName;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @return 手机号，不一定能拿到
     */
    @SuppressLint("MissingPermission")
    public static String getPhoneNumber(Context context) {
        String phoneNumber = null;
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (tm != null) {
            phoneNumber = tm.getLine1Number();
        }
        return phoneNumber;
    }

    /**
     * 安装应用完成点击打开和从桌面启动程序Intent不同，可能导致重新实例化入口类的activity
     *
     * @param activity
     * @param intent
     * @return
     */
    public static boolean duplicateStartup(Activity activity, Intent intent) {
        // 避免从桌面启动程序后，会重新实例化入口类的activity
        // 当前类不是该Task的根部，那么之前启动
        if (!activity.isTaskRoot()) {
            if (intent != null) {
                String action = intent.getAction();
                // 当前类是从桌面启动的,如果启动的Activity被放在后台已经存在的应用task栈上并将这个task栈带到前台，Intent中就会带有Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
                if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(action) &&
                        ((intent.getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0)) {
                    activity.finish(); // finish掉该类，直接打开该Task中现存的Activity
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 获取进程名字
     * @param cxt
     * @param pid
     * @return
     */
    public static String getProcessName(@NonNull Context cxt, int pid) {
        ActivityManager am = (ActivityManager) cxt.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningApps = am.getRunningAppProcesses();
        if (runningApps == null) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo procInfo : runningApps) {
            if (procInfo.pid == pid) {
                return procInfo.processName;
            }
        }
        return null;
    }

    /**
     * dp转px
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * px转dp
     * @param context
     * @param pxValue
     * @return
     */
    public static int px2dip(Context context, int pxValue) {
        return ((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, pxValue, context.getResources().getDisplayMetrics()));
    }

    /**
     * 32位MD5加密
     *
     * @param content -- 待加密内容
     * @return
     */
    public static String md5Decode(String content) {
        byte[] hash;
        try {
            hash = MessageDigest.getInstance("MD5").digest(content.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("NoSuchAlgorithmException", e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UnsupportedEncodingException", e);
        }
        //对生成的16字节数组进行补零操作
        StringBuilder hex = new StringBuilder(hash.length * 2);
        for (byte b : hash) {
            if ((b & 0xFF) < 0x10) {
                hex.append("0");
            }
            hex.append(Integer.toHexString(b & 0xFF));
        }
        return hex.toString();
    }

    /**
     * 获得独一无二的Psuedo ID
     */
    public static String getUniquePsuedoID() {
        String serial = null;

        String m_szDevIDShort = "35" +
                Build.BOARD.length() % 10 + Build.BRAND.length() % 10 +

                Build.CPU_ABI.length() % 10 + Build.DEVICE.length() % 10 +

                Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 +

                Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 +

                Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10 +

                Build.TAGS.length() % 10 + Build.TYPE.length() % 10 +

                Build.USER.length() % 10; //13 位

        try {
            serial = Build.class.getField("SERIAL").get(null).toString();
            //API>=9 使用serial号
            return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        } catch (Exception exception) {
            //serial需要一个初始化
            serial = "serial";
        }
        //使用硬件信息拼凑出来的15位号码
        return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
    }

    /**
     * 判断厂商
     * @return
     */
    public static boolean isMeiZu() {
        //这个字符串可以自己定义,例如判断华为就填写huawei,魅族就填写meizu
        return "meizu".equalsIgnoreCase(Build.MANUFACTURER);
    }

    public static boolean isXiaoMi() {
        return "xiaomi".equalsIgnoreCase(Build.MANUFACTURER);
    }

    public static boolean isOppo() {
        return "oppo".equalsIgnoreCase(Build.MANUFACTURER);
    }

    public static boolean isVivo() {
        return "vivo".equalsIgnoreCase(Build.MANUFACTURER);
    }

    public static boolean isSamsung() {
        return "samsung".equalsIgnoreCase(Build.MANUFACTURER);
    }

    /**
     * 使activity透明
     * @param activity
     */
    public static void translucentActivity(Activity activity) {
        try {
            activity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            activity.getWindow().getDecorView().setBackground(null);
            Method activityOptions = Activity.class.getDeclaredMethod("getActivityOptions");
            activityOptions.setAccessible(true);
            Object options = activityOptions.invoke(activity);
            Class<?>[] classes = Activity.class.getDeclaredClasses();
            Class<?> aClass = null;
            for (Class clazz : classes) {
                if (clazz.getSimpleName().contains("TranslucentConversionListener")) {
                    aClass = clazz;
                }
            }
            Method method = Activity.class.getDeclaredMethod("convertToTranslucent",
                    aClass, ActivityOptions.class);
            method.setAccessible(true);
            method.invoke(activity, null, options);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * 获取当前App所有进程
     */
    public List<ActivityManager.RunningAppProcessInfo> getRunningAppProcessInfos(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        return am.getRunningAppProcesses();
    }

    /**
     * 判断该进程ID是否属于该进程名
     * @param context
     * @param pid 进程ID
     * @param name 进程名
     * @return true属于该进程名
     */
    public static boolean isPidOfProcessName(Context context, int pid, String name) {
        if (name == null) {
            return false;
        }
        boolean isMain = false;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        //遍历所有进程
        for (ActivityManager.RunningAppProcessInfo process : am.getRunningAppProcesses()) {
            if (process.pid == pid) {
                //进程ID相同时判断该进程名是否一致
                if (process.processName.equals(name)) {
                    isMain = true;
                }
                break;
            }
        }
        return isMain;
    }

    /**
     * 获取主进程名
     * @param context 上下文
     * @return 主进程名
     */
    public static String getMainProcessName(Context context) throws PackageManager.NameNotFoundException {
        return context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).processName;
    }

    /**
     * 判断是否主进程
     * @param context 上下文
     * @return true是主进程
     */
    public static boolean isMainProcess(Context context) {
        boolean process = false;
        try {
            process = isPidOfProcessName(context, android.os.Process.myPid(), getMainProcessName(context));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return process;
    }

    /**
     * 检测某Activity是否在当前Task的栈顶
     * appointClassName：指定类名称
     */
    public static boolean isTopActivity(String appointClassName, Context context) {
        try {
            ActivityManager manager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> runningTaskInfos = manager.getRunningTasks(1);
            String topClassName = null;
            if (null != runningTaskInfos) {
                topClassName = (runningTaskInfos.get(0).topActivity.getShortClassName()).toString();
            }
            if (TextUtils.isEmpty(topClassName)) {
                return false;
            }
            return topClassName.contains(appointClassName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
