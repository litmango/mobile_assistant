package com.kylin.mobileassistant.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.data.MusicInfo;
import com.kylin.mobileassistant.presenter.transferfiles.TransferFilesActivityPresenter;
import com.kylin.mobileassistant.utils.TransformUtil;

import java.util.ArrayList;

public class MusicListAdapter extends BaseListAdapter {

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        MusicInfo musicInfo = (MusicInfo)getItem(position);
        View view = LayoutInflater.from(getContext()).inflate(id, parent, false);
        ImageView checkBox = view.findViewById(R.id.check_box_5);
        if (position >= firstVisible && position < firstVisible + visibleNum) {
            TextView textView = view.findViewById(R.id.piece_music_name);
            textView.setText(musicInfo.getName());
            TextView textView1 = view.findViewById(R.id.piece_music_duration);
            textView1.setText(musicInfo.getDuration());
            TextView textView2 = view.findViewById(R.id.piece_music_size);
            textView2.setText(TransformUtil.turnByteToOther(musicInfo.getSize()));
            checkBox.setImageBitmap(TransferFilesActivityPresenter.getUncheckedIcon());
        }
        if (selectedIndex.contains(position)) {
            checkBox.setImageBitmap(TransferFilesActivityPresenter.getCheckedIcon());
        }
        return view;
    }

    public MusicListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<MusicInfo> musicInfos) {
        super(context, resource, musicInfos);
    }
}
