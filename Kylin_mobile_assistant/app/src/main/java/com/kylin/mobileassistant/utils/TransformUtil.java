package com.kylin.mobileassistant.utils;

import java.util.Locale;

public class TransformUtil {
    public static String turnSecondsToHours(long seconds) {
        if(seconds < 60) {
            return "00:" + String.format(Locale.CHINA,"%02d", seconds);
        }
        if(seconds < 3600) {
            return String.format(Locale.CHINA,"%02d", seconds / 60) + ":" +
                    String.format(Locale.CHINA, "%02d", seconds % 60);
        }
        return String.format(Locale.CHINA, "%02d", seconds / 3600) + ":" +
                String.format(Locale.CHINA, "%02d", (seconds % 3600) / 60) + ":" +
                String.format(Locale.CHINA, "%02d", seconds % 60);
    }

    public static String turnByteToOther(long bytes) {
        if(bytes < 1024) {
            return bytes + "B";
        }
        double size = bytes;
        size = Math.round(size / 1024);
        if(size < 1024) {
            return (int)size + "KB";
        }
        size /= 1024;
        if(size < 1024) {
            size = Math.round(size * 100);
            size /= 100;
            return size + "MB";
        }
        size /= 1024;
        size = Math.round(size * 100);
        size /= 100;
        return size + "GB";
    }
}
