package com.kylin.mobileassistant.ui.activity.multiscreeninteraction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.base.view.BaseMvpActivity;
import com.kylin.mobileassistant.presenter.multiscreeninteraction.ConnectedActivityPresenter;
import com.kylin.mobileassistant.ui.activity.interfaces.multiscreeninteraction.IConnectedActivity;
import com.kylin.mobileassistant.ui.activity.main.MainActivity;
import com.kylin.mobileassistant.utils.AppUtils;

public class ConnectedActivity extends BaseMvpActivity<ConnectedActivityPresenter>
        implements IConnectedActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connected);
        initView();
    }

    private void initView() {
        findViewById(R.id.unconnect_to_computer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectedActivity.this.finish();
            }
        });
    }

    @Override
    protected void initPresenter() {
        mPresenter = new ConnectedActivityPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_connected;
    }
}
