package com.kylin.mobileassistant.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;

import com.kylin.mobileassistant.data.BaseInfo;

import java.util.ArrayList;

public class BaseListAdapter<T extends BaseInfo> extends ArrayAdapter<T> {
    protected final int id;
    protected int firstVisible = 0;
    protected int visibleNum = 36;
    protected ArrayList<Integer> selectedIndex = new ArrayList<>();

    public BaseListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<T> infos) {
        super(context, resource, infos);
        id = resource;
    }

    public int size() {
        return selectedIndex.size();
    }

    public boolean isContained(Integer i) {
        return selectedIndex.contains(i);
    }

    public void selecting(Integer i) {
        selectedIndex.add(i);
    }

    public void unSelecting(Integer i) {
        selectedIndex.remove(i);
    }

    public void setFirstVisible(int i) {
        firstVisible = i;
    }

    public void setVisibleNum(int i) {
        if(i != 0) {
            visibleNum = i;
        }
    }

        public int getFirstVisible() {
        return firstVisible;
    }
}
