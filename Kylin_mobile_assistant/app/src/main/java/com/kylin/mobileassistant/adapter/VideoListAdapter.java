package com.kylin.mobileassistant.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.data.VideoInfo;
import com.kylin.mobileassistant.presenter.transferfiles.TransferFilesActivityPresenter;
import com.kylin.mobileassistant.utils.TransformUtil;

import java.io.IOException;
import java.util.ArrayList;

public class VideoListAdapter extends BaseListAdapter {
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        VideoInfo videoInfo = (VideoInfo)getItem(position);
        View view = LayoutInflater.from(getContext()).inflate(id, parent, false);
        ImageView checkBox = view.findViewById(R.id.check_box_4);
        if (position >= firstVisible && position < firstVisible + visibleNum) {
            Bitmap bitmap = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                try {
                    bitmap = getContext().getContentResolver().loadThumbnail(videoInfo.getThumbUri(), new Size(200, 200), null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            ImageView imageView = view.findViewById(R.id.piece_video_image);
            imageView.setImageBitmap(bitmap);
            TextView textView = view.findViewById(R.id.piece_video_name);
            textView.setText(videoInfo.getName());
            TextView textView1 = view.findViewById(R.id.piece_video_duration);
            textView1.setText(videoInfo.getDuration());
            TextView textView2 = view.findViewById(R.id.piece_video_size);
            textView2.setText(TransformUtil.turnByteToOther(videoInfo.getSize()));
            checkBox.setImageBitmap(TransferFilesActivityPresenter.getUncheckedIcon());
        }
        if (selectedIndex.contains(position)) {
            checkBox.setImageBitmap(TransferFilesActivityPresenter.getCheckedIcon());
        }
        return view;
    }

    public VideoListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<VideoInfo> videoInfos) {
        super(context, resource, videoInfos);
    }
}
