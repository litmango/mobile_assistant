package com.kylin.mobileassistant.base.presenter;

import android.os.Bundle;

import com.fewlaps.quitnowcache.QNCache;
import com.fewlaps.quitnowcache.QNCacheBuilder;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author : zhaoyang
 * @date : 2020/7/30
 */
public class PresenterManager {
    private static final String KEY_PRESENTER_ID = "presenter_id";

    private final AtomicLong currentId;
    private final QNCache<BasePresenter<?>> qnCache;

    public PresenterManager() {
        currentId = new AtomicLong();
        qnCache = new QNCacheBuilder().createQNCache();
    }

    public <P extends BasePresenter<?>> P restorePresenter(Bundle savedInstanceState) {
        return restorePresenter(savedInstanceState, KEY_PRESENTER_ID);
    }

    public <P extends BasePresenter<?>> P restorePresenter(Bundle savedInstanceState, String key) {
        Long presenterId = savedInstanceState.getLong(key);
        P presenter = (P) qnCache.get(presenterId.toString());
        qnCache.remove(presenterId.toString());
        return presenter;
    }


    public boolean containsPresenter(Bundle savedInstanceState) {
        return containsPresenter(savedInstanceState, KEY_PRESENTER_ID);
    }

    public boolean containsPresenter(Bundle savedInstanceState, String key) {
        Long presenterId = savedInstanceState.getLong(key);
        return qnCache.contains(presenterId.toString());
    }

    public void savePresenter(BasePresenter<?> presenter, Bundle outState) {
        savePresenter(presenter, outState, KEY_PRESENTER_ID);
    }

    public void savePresenter(BasePresenter<?> presenter, Bundle outState, String key) {
        Long presenterId = currentId.incrementAndGet();
        qnCache.set(presenterId.toString(), presenter);
        outState.putLong(key, presenterId);
    }
}
