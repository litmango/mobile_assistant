package com.kylin.mobileassistant.presenter.transferfiles;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.adapter.ApplicationListAdapter;
import com.kylin.mobileassistant.base.presenter.BasePresenter;
import com.kylin.mobileassistant.ui.activity.interfaces.transferfiles.IApplicationsFragment;
import com.kylin.mobileassistant.ui.activity.transferfiles.TransferFilesActivity;

import java.util.ArrayList;
import java.util.List;

public class ApplicationsFragmentPresenter extends BasePresenter<IApplicationsFragment> {
    private ArrayList<PackageInfo> applications = new ArrayList<>();
    private ApplicationListAdapter applicationListAdapter = null;

    public void updateView(TransferFilesActivity transferFilesActivity) {
        TextView selectedNumView = transferFilesActivity.findViewById(R.id.text_select);
        String text = "已选择" + applicationListAdapter.size() + "项";
        selectedNumView.setText(text);
        applicationListAdapter.notifyDataSetChanged();
    }

    public void loadApplicationView(View view, TransferFilesActivity transferFilesActivity) {
        List<PackageInfo> packageInfos = view.getContext().getPackageManager().getInstalledPackages(
                PackageManager.GET_ACTIVITIES | PackageManager.GET_SERVICES);
        applications.addAll(packageInfos);
        applicationListAdapter = new ApplicationListAdapter(view.getContext(),
                        R.layout.piece_appliction, applications);
        GridView gridView = view.findViewById(R.id.applications_view);
        gridView.setAdapter(applicationListAdapter);
        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if(scrollState == SCROLL_STATE_IDLE) {
                    applicationListAdapter.notifyDataSetChanged();
                    transferFilesActivity.setViewPagerSlide(true);
                } else {
                    transferFilesActivity.setViewPagerSlide(false);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                applicationListAdapter.setFirstVisible(firstVisibleItem);
                applicationListAdapter.setVisibleNum(visibleItemCount);
            }
        });
        gridView.setOnItemClickListener((parent, view1, position, id) -> {
            View child = gridView.getChildAt(position - applicationListAdapter.getFirstVisible());
            ImageView imageView = child.findViewById(R.id.check_box_1);
            if(applicationListAdapter.isContained(position)) {
                applicationListAdapter.unSelecting(position);
                imageView.setImageBitmap(TransferFilesActivityPresenter.getUncheckedIcon());
            } else {
                applicationListAdapter.selecting(position);
                imageView.setImageBitmap(TransferFilesActivityPresenter.getCheckedIcon());
            }
            TextView selectedNumView = transferFilesActivity.findViewById(R.id.text_select);
            String text = "已选择" + applicationListAdapter.size() + "项";
            selectedNumView.setText(text);
        });
    }
}
