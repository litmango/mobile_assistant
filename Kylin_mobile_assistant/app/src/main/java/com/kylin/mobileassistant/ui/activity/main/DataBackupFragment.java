package com.kylin.mobileassistant.ui.activity.main;

import android.content.Context;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.base.view.BaseMvpFragment;
import com.kylin.mobileassistant.presenter.main.DataBackupFragmentPresenter;
import com.kylin.mobileassistant.ui.activity.interfaces.main.IDataBackupFragment;

public class DataBackupFragment extends BaseMvpFragment<DataBackupFragmentPresenter>
        implements IDataBackupFragment {
    private MainActivity parent = null;

    @Override
    public void onAttach(@NonNull Context context) {
        parent = (MainActivity)context;
        super.onAttach(context);
    }

    @Override
    protected void initPresenter() {
        mPresenter = new DataBackupFragmentPresenter();
    }

    @Override
    public void onResume() {
        TextView textView = parent.findViewById(R.id.top_text);
        textView.setText("数据备份");
        super.onResume();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_data_backup;
    }
}
