package com.kylin.mobileassistant.ui.activity.main;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import com.huawei.hms.ml.scan.HmsScan;
import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.base.view.BaseMvpActivity;
import com.kylin.mobileassistant.presenter.main.MainActivityPresenter;
import com.kylin.mobileassistant.ui.activity.interfaces.main.IMainActivityView;
import com.kylin.mobileassistant.utils.AppUtils;

public class MainActivity extends BaseMvpActivity<MainActivityPresenter>
        implements IMainActivityView,
        BottomNavigationView.OnNavigationItemSelectedListener{
    private ViewPager2 functionsViewPager;
    private final int REQUEST_CODE_SCAN = 200;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initPresenter() {
        mPresenter = new MainActivityPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppUtils.verifyStoragePermissions(this);
        initView();
    }

    public void initView() {
        final TransferFilesFragment transferFilesFragment = new TransferFilesFragment();
        final DataBackupFragment dataBackupFragment = new DataBackupFragment();
        final MultiScreenInteractionFragment multiScreenInteractionFragment
                = new MultiScreenInteractionFragment();
        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        functionsViewPager = findViewById(R.id.main_view_pager);
        functionsViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int item) {
                super.onPageSelected(item);
                bottomNavigationView.getMenu().getItem(item).setChecked(true);
            }
        });
        functionsViewPager.setAdapter(new FragmentStateAdapter(this) {
            @NonNull
            @Override
            public Fragment createFragment(int position) {
                switch (position) {
                    case 0:
                        return multiScreenInteractionFragment;
                    case 1:
                        return dataBackupFragment;
                    case 2:
                        return transferFilesFragment;
                }
                return multiScreenInteractionFragment;
            }

            @Override
            public int getItemCount() {
                return 3;
            }
        });
        findViewById(R.id.scan_qr).setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, ScanQRActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        functionsViewPager.setCurrentItem(item.getOrder());
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("android:support:fragments", null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK || data == null) {
            System.out.println(requestCode);
            return;
        }
        if(requestCode == REQUEST_CODE_SCAN){
            //获取扫码结果
            HmsScan hmsScan=data.getParcelableExtra(ScanQRActivity.SCAN_RESULT);
            if(hmsScan != null && !TextUtils.isEmpty(hmsScan.getOriginalValue())){
                Toast.makeText(MainActivity.this, hmsScan.getOriginalValue(), Toast.LENGTH_LONG).show();
            }
        }
    }
}