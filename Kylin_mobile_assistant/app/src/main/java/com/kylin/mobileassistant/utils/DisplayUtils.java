package com.kylin.mobileassistant.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import java.util.List;

/**
 * 提供显示相关功能，如屏幕尺寸，toast显示，键盘显示/隐藏
 */
public class DisplayUtils {
    private static Toast sToast;

    /**
     * 获得屏幕宽度
     *
     * @param context
     * @return
     */
    public static int getScreenWidth(Context context) {
        DisplayMetrics outMetrics = new DisplayMetrics();
        getDisplay(context).getMetrics(outMetrics);
        return outMetrics.widthPixels;
    }

    /**
     * 获得屏幕高度
     *
     * @param context
     * @return
     */
    public static int getScreenHeight(Context context) {
        DisplayMetrics outMetrics = new DisplayMetrics();
        getDisplay(context).getMetrics(outMetrics);
        return outMetrics.heightPixels;
    }

    /**
     * 获取Display
     *
     * @param context
     * @return
     */
    public static Display getDisplay(Context context) {
        WindowManager manager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        return manager.getDefaultDisplay();
    }

    /**
     * 显示软键盘
     *
     * @param context Instance of {@link Context}
     * @param view    The instance of {@link View}
     */
    public static void showSoftKeyboardForView(Context context, View view) {
        if (view != null || context != null) {
            final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        } else {
            Log.w("TAG", "View or context is null. Could not show keyboard.");
        }
    }

    /**
     * 隐藏键盘（推荐使用）
     *
     * @param context Instance of {@link Context}
     * @param view    The instance of {@link View}
     */
    public static void hideSoftKeyboardForView(Context context, View view) {
        if (view != null && context != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } else {
            Log.w("TAG", "View or context is null. Could not hide keyboard.");
        }
    }

    /**
     * 隐藏键盘
     */
    public static void hideKeyboard(Context context) {
        if (context != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);

            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        } else {
            Log.w("TAG", "Context is null. Could not hide keyboard.");
        }
    }

    /**
     * Returns display dimensions in pixels for the device.
     *
     * @param c Instance of {@link Context}
     * @return Instance of {@link Point}. X contains width in px, Y contains height in px
     */
    public static Point getDisplayDimensions(Context c) {
        WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point result = new Point();
        display.getSize(result);
        return result;
    }


    /**
     * 根据view和偏移，确定toast的位置
     * @param view
     * @param context
     * @param strMsg
     * @param shift
     * @param isLong
     */
    public synchronized static void showToastRelativeToView(@NonNull View view, Context context, @NonNull String strMsg, int shift, boolean isLong) {
        if (sToast != null) {
            sToast.cancel();
        }
        sToast = Toast.makeText(context, strMsg, isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
        int[] position = new int[2];
        view.getLocationInWindow(position);
        sToast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0, position[1] + shift);
        sToast.show();
    }

    public synchronized static void showToastRelativeToView(@NonNull View view, Context context, @StringRes int strRes, int shift, boolean isLong) {
        showToastRelativeToView(view, context, context.getString(strRes), shift, isLong);
    }

    public static void showToastRelativeToView(@NonNull View view, Context context, List<Integer> errors, int shift, boolean isLong) {
        StringBuilder concatErrors = new StringBuilder();
        for (Integer error : errors) {
            concatErrors.append(context.getString(error));
            concatErrors.append("\n");
        }
        showToastRelativeToView(view, context, concatErrors.toString(), shift, isLong);
    }

    //获取屏幕宽高
    public static int getScreenWidthInPixels(Dialog dialog) {
        Display display = dialog.getWindow().getWindowManager().getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        return point.x;
    }

    //获取屏幕宽高
    public static float getScreenDpi(Dialog dialog) {
        Display display = dialog.getWindow().getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        return metrics.density;
    }

}
