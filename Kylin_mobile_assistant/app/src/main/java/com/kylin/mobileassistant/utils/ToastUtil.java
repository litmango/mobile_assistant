package com.kylin.mobileassistant.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.NotificationManagerCompat;

import com.kylin.mobileassistant.R;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author : zhaoyang
 * @date : 2020/3/16
 * 显示自定义的不同样式的toast：0 normal，1 success，2 error，3 warn
 */
public class ToastUtil {
    private static Object iNotificationManagerObj;

    /**
     * 底部显示普通toast
     *
     * @param context
     * @param message
     */
    public static void show(Context context, String message) {
        show(context, message, Toast.LENGTH_SHORT, Gravity.BOTTOM, 0, 0, AppUtils.px2dip(context, 100));
    }

    /**
     * 中间显示普通toast
     *
     * @param context
     * @param message
     */
    public static void showCenter(Context context, String message) {
        show(context, message, Toast.LENGTH_SHORT, Gravity.CENTER, 0, 0, 0);
    }

    /**
     * 底部显示普通toast
     *
     * @param context
     * @param message
     */
    public static void showBottom(Context context, String message) {
        show(context, message, Toast.LENGTH_SHORT, Gravity.BOTTOM, 0, 0, AppUtils.px2dip(context, 100));
    }

    /**
     * 不同样式的toast
     *
     * @param context
     * @param message
     * @param type    0 normal，1 success，2 error，3 warn
     */
    public static void showCenter(Context context, String message, int type) {
        show(context, message, Toast.LENGTH_SHORT, Gravity.CENTER, type, 0, 0);
    }

    /**
     * 底部不同样式的toast
     *
     * @param context
     * @param message
     * @param type    0 normal，1 success，2 error，3 warn
     */
    public static void showBottom(Context context, String message, int type) {
        show(context, message, Toast.LENGTH_SHORT, Gravity.BOTTOM, type, 0, AppUtils.px2dip(context, 100));
    }

    /**
     * @param context
     * @param message
     */
    public static void show(Context context, String message, int duration, int gravity, int type, int xOffset, int yOffset) {
        if (TextUtils.isEmpty(message)) {
            return;
        }
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Toast toast = new Toast(context);
        View view = inflate.inflate(R.layout.base_layout_toast, null);
        TextView tv = (TextView) view.findViewById(R.id.tv_message);
        tv.setText(message);
        toast.setView(view);
        toast.setDuration(duration);
        toast.setGravity(gravity, xOffset, yOffset);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
            toast.show();
        } else {
            if (isNotificationEnabled(context)) {
                toast.show();
            } else {
                showSystemToast(toast);
            }
        }
    }

    /**
     * 业务相关的toast
     *
     * @param context
     * @param message
     */
    public static void showCenterForBusiness(final Context context, final String message, int gravity) {
        if (context != null) {
            if (context instanceof Activity) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        show(context, message, Toast.LENGTH_SHORT, gravity, 0, 0, gravity == Gravity.CENTER ? 0 : AppUtils.px2dip(context, 100));
                    }
                });
            } else {
                show(context, message, Toast.LENGTH_SHORT, gravity, 0, 0, gravity == Gravity.CENTER ? 0 : AppUtils.px2dip(context, 100));
            }
        }
    }

    /**
     * 显示系统Toast
     */
    private static void showSystemToast(Toast toast) {
        try {
            @SuppressLint("SoonBlockedPrivateApi") Method getServiceMethod = Toast.class.getDeclaredMethod("getService");
            getServiceMethod.setAccessible(true);
            //hook INotificationManager
            if (iNotificationManagerObj == null) {
                iNotificationManagerObj = getServiceMethod.invoke(null);
                Class iNotificationManagerCls = Class.forName("android.app.INotificationManager");
                Object iNotificationManagerProxy = Proxy.newProxyInstance(toast.getClass().getClassLoader(), new Class[]{iNotificationManagerCls}, new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        //强制使用系统Toast
                        if ("enqueueToast".equals(method.getName()) || "enqueueToastEx".equals(method.getName())) {
                            //华为p20 pro上为enqueueToastEx
                            args[0] = "android";
                        }
                        return method.invoke(iNotificationManagerObj, args);
                    }
                });
                Field sServiceFiled = Toast.class.getDeclaredField("sService");
                sServiceFiled.setAccessible(true);
                sServiceFiled.set(null, iNotificationManagerProxy);
            }
            toast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 消息通知是否开启
     *
     * @return
     */
    private static boolean isNotificationEnabled(Context context) {
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        boolean areNotificationsEnabled = notificationManagerCompat.areNotificationsEnabled();
        return areNotificationsEnabled;
    }
}
