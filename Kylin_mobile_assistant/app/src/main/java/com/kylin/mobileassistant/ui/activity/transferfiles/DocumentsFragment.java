package com.kylin.mobileassistant.ui.activity.transferfiles;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.base.view.BaseMvpFragment;
import com.kylin.mobileassistant.presenter.transferfiles.DocumentsFragmentPresenter;
import com.kylin.mobileassistant.ui.activity.interfaces.transferfiles.IDocumentsFragment;

public class DocumentsFragment extends BaseMvpFragment<DocumentsFragmentPresenter>
        implements IDocumentsFragment {
    private TransferFilesActivity parent = null;

    @Override
    public void onAttach(@NonNull Context context) {
        parent = (TransferFilesActivity)context;
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.loadDocumentsView(view, parent);
    }

    @Override
    protected void initPresenter() {
        mPresenter = new DocumentsFragmentPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_send_documents;
    }
}
