package com.kylin.mobileassistant.presenter.main;

import com.kylin.mobileassistant.base.presenter.BasePresenter;
import com.kylin.mobileassistant.ui.activity.interfaces.main.IMultiScreenInteractionFragment;

public class MultiScreenInteractionFragmentPresenter extends BasePresenter<IMultiScreenInteractionFragment> {
}
