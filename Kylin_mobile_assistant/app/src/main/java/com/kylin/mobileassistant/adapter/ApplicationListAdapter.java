package com.kylin.mobileassistant.adapter;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.presenter.transferfiles.TransferFilesActivityPresenter;

import java.util.ArrayList;

public class ApplicationListAdapter extends BaseListAdapter {
    public ApplicationListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<PackageInfo> packages) {
        super(context, resource, packages);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PackageInfo application = (PackageInfo)getItem(position);
        View view = LayoutInflater.from(getContext()).inflate(id, parent, false);
        ImageView checkBox = view.findViewById(R.id.check_box_1);
        if(position >= firstVisible && position < firstVisible + visibleNum) {
            ImageView imageView = view.findViewById(R.id.piece_application_view);
            imageView.setImageDrawable(application.applicationInfo.loadIcon(getContext().getPackageManager()));
            checkBox.setImageBitmap(TransferFilesActivityPresenter.getUncheckedIcon());
            TextView textView = view.findViewById(R.id.applications_name);
            textView.setText(application.applicationInfo.loadLabel(getContext().getPackageManager()));
        }
        if(selectedIndex.contains(position)) {
            checkBox.setImageBitmap(TransferFilesActivityPresenter.getCheckedIcon());
        }
        return view;
    }
}
