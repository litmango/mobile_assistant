package com.kylin.mobileassistant.presenter.transferfiles;

import android.content.ContentUris;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.adapter.VideoListAdapter;
import com.kylin.mobileassistant.base.presenter.BasePresenter;
import com.kylin.mobileassistant.data.VideoInfo;
import com.kylin.mobileassistant.ui.activity.interfaces.transferfiles.IVideosFragment;
import com.kylin.mobileassistant.ui.activity.transferfiles.TransferFilesActivity;

import java.util.ArrayList;

public class VideosFragmentPresenter extends BasePresenter<IVideosFragment> {
    private final ArrayList<VideoInfo> videos = new ArrayList<>();
    private VideoListAdapter videoListAdapter = null;

    public void loadVideoView(View view, TransferFilesActivity transferFilesActivity) {
        getVideosInfo(view);
        videoListAdapter = new VideoListAdapter(view.getContext(), R.layout.piece_video, videos);
        ListView listView = view.findViewById(R.id.videos_view);
        listView.setAdapter(videoListAdapter);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if(scrollState == SCROLL_STATE_IDLE) {
                    videoListAdapter.notifyDataSetChanged();
                    transferFilesActivity.setViewPagerSlide(true);
                } else {
                    transferFilesActivity.setViewPagerSlide(false);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                videoListAdapter.setFirstVisible(firstVisibleItem);
                videoListAdapter.setVisibleNum(visibleItemCount);
            }
        });
        listView.setOnItemClickListener((parent, view1, position, id) -> {
            View child = listView.getChildAt(position - videoListAdapter.getFirstVisible());
            ImageView imageView = child.findViewById(R.id.check_box_4);
            if(videoListAdapter.isContained(position)) {
                videoListAdapter.unSelecting(position);
                imageView.setImageBitmap(TransferFilesActivityPresenter.getUncheckedIcon());
            } else {
                videoListAdapter.selecting(position);
                imageView.setImageBitmap(TransferFilesActivityPresenter.getCheckedIcon());
            }
            TextView selectedNumView = transferFilesActivity.findViewById(R.id.text_select);
            String text = "已选择" + videoListAdapter.size() + "项";
            selectedNumView.setText(text);
        });
    }

    public void getVideosInfo(View view) {
        Cursor cursor = view.getContext().getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI, null, null,
                null, MediaStore.Video.Media.DATE_MODIFIED + " DESC"
        );
        int nameIndex = cursor.getColumnIndex(MediaStore.Video.Media.DISPLAY_NAME);
        int pathIndex = cursor.getColumnIndex(MediaStore.Video.Media.DATA);
        int durationIndex = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            durationIndex = cursor.getColumnIndex(MediaStore.Video.Media.DURATION);
        }
        int thumbIndex = cursor.getColumnIndex(MediaStore.Video.VideoColumns._ID);
        int sizeIndex = cursor.getColumnIndex(MediaStore.Video.Media.SIZE);
        for(cursor.moveToFirst();cursor.moveToNext();) {
            int thumpId = cursor.getInt(thumbIndex);
            String path = cursor.getString(pathIndex);
            String name = cursor.getString(nameIndex);
            Uri thumbUri = ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, thumpId);
            long duration;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                duration = cursor.getLong(durationIndex);
            } else {
                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(path);
                duration = Integer.parseInt(mmr.extractMetadata
                        (MediaMetadataRetriever.METADATA_KEY_DURATION));
            }
            long size = cursor.getLong(sizeIndex);
            if(duration > 0) {
                videos.add(new VideoInfo(path, name, thumbUri, duration, size));
            }
        }
        cursor.close();
    }

    public void updateView(TransferFilesActivity transferFilesActivity) {
        TextView selectedNumView = transferFilesActivity.findViewById(R.id.text_select);
        String text = "已选择" + videoListAdapter.size() + "项";
        selectedNumView.setText(text);
        videoListAdapter.notifyDataSetChanged();
    }
}
