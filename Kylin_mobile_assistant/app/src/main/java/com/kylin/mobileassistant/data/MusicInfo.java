package com.kylin.mobileassistant.data;

import android.net.Uri;

import com.kylin.mobileassistant.utils.TransformUtil;

public class MusicInfo extends BaseInfo {
    private final long duration;

    public MusicInfo(String path, String name, long size, long duration) {
        super(path, name, size);
        this.duration = duration;
    }

    public String getDuration() {
        return TransformUtil.turnSecondsToHours(duration / 1000);
    }
}
