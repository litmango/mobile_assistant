package com.kylin.mobileassistant.ui.activity.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.base.view.BaseMvpFragment;
import com.kylin.mobileassistant.presenter.main.TransferFilesFragmentPresenter;
import com.kylin.mobileassistant.ui.activity.interfaces.main.ITransferFilesFragment;
import com.kylin.mobileassistant.ui.activity.transferfiles.TransferFilesActivity;

public class TransferFilesFragment extends BaseMvpFragment<TransferFilesFragmentPresenter>
        implements ITransferFilesFragment {
    private MainActivity parent = null;

    @Override
    public void onAttach(@NonNull Context context) {
        parent = (MainActivity)context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflateView(inflater, container);
        Button sendFilesBtn = view.findViewById(R.id.send_files_btn);
        sendFilesBtn.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(getActivity(), TransferFilesActivity.class);
            startActivity(intent);
        });
        return view;
    }
    @Override
    protected void initPresenter() {
        mPresenter = new TransferFilesFragmentPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_transfer_files;
    }

    @Override
    public void onResume() {
        TextView textView = parent.findViewById(R.id.top_text);
        textView.setText("传输文件");
        super.onResume();
    }
}
