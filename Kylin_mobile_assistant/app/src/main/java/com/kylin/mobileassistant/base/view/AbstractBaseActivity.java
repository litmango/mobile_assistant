package com.kylin.mobileassistant.base.view;

import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.kylin.mobileassistant.utils.ActivityFinishUtil;

import butterknife.ButterKnife;

public abstract class AbstractBaseActivity extends AppCompatActivity {

    private static float sNoncompatDensity;
    private static float sNoncompatScaledDensity;

    public static final String ACTION_GENERIC_ERROR = "ACTION_GENERIC_ERROR";

    private boolean mIsVisible;

    BroadcastReceiver genericErrorReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //showGenericErrorDialog();
            Log.d("TAG", "generic error broadcast received");
        }
    };

    @LayoutRes
    protected abstract int getLayoutId();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //for samsung
        setCustomDensity(this, this.getApplication());
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        ActivityFinishUtil.addActivity(this);
        setStatusBarAndWindow();
    }

    /**
     * Override to show back button in Toolbar
     *
     * @return
     */
    protected boolean showToolbarBackButton() {
        return false;
    }

    /**
     * 默认使深色主题状态栏，可重写改变状态栏主题色
     * @return
     */
    protected boolean isLightStatusBar() {
        return true;
    }

    private void setStatusBarAndWindow() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        View decor = getWindow().getDecorView();
        if (isLightStatusBar()) {
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        }
    }
    /**
     * 设置状态栏浅色主题
     */
    protected void setLightThemeStatusBar() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            View decor = window.getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     * 屏蔽更改文字大小和屏幕大小对app布局错乱的影响
     * 是解决方案，但是同样给人感觉是更改文字大小/屏幕大小未生效
     * 据简单观察微信/宝宝/京东/淘宝等布局密集应用采用类似方案
     * @return Resources
     */
    private static void setCustomDensity(@NonNull AppCompatActivity activity, @NonNull final android.app.Application application) {
        DisplayMetrics appDisplayMetrics = application.getResources().getDisplayMetrics();
        if (sNoncompatDensity == 0) {
            sNoncompatDensity = appDisplayMetrics.density;
            sNoncompatScaledDensity = appDisplayMetrics.scaledDensity;
            application.registerComponentCallbacks(new ComponentCallbacks() {
                @Override
                public void onConfigurationChanged(Configuration newConfig) {
                    if (newConfig != null && newConfig.fontScale > 0) {
                        sNoncompatScaledDensity = application.getResources().getDisplayMetrics().scaledDensity;
                    }
                }

                @Override
                public void onLowMemory() {

                }
            });
        }
        float targetDensity = appDisplayMetrics.widthPixels / 360;
        float targetScaledDensity = targetDensity * (sNoncompatScaledDensity / sNoncompatDensity);
        int targetDensityDpi = (int) (160 * targetDensity);

        appDisplayMetrics.density = targetDensity;
        appDisplayMetrics.scaledDensity = targetScaledDensity;
        appDisplayMetrics.densityDpi = targetDensityDpi;

        DisplayMetrics activityDisplayMetrics = activity.getResources().getDisplayMetrics();
        activityDisplayMetrics.density = targetDensity;
        activityDisplayMetrics.scaledDensity = targetScaledDensity;
        activityDisplayMetrics.densityDpi = targetDensityDpi;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsVisible = true;
        registers();
    }

    private void registers() {
        //register lock generic error
        IntentFilter filter = new IntentFilter(ACTION_GENERIC_ERROR);
        registerReceiver(genericErrorReceiver, filter);
    }

    @Override
    protected void onPause() {
        mIsVisible = false;
        unregisters();
        super.onPause();
    }

    private void unregisters() {
        unregisterReceiver(genericErrorReceiver);
    }

    /**
     * @return true activity 可见
     */
    public boolean isVisible() {
        return mIsVisible;
    }

}
