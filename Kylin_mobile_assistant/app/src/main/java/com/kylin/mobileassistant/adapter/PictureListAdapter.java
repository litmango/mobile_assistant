package com.kylin.mobileassistant.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.data.PictureInfo;
import com.kylin.mobileassistant.presenter.transferfiles.TransferFilesActivityPresenter;

import java.io.IOException;
import java.util.ArrayList;

public class PictureListAdapter extends BaseListAdapter {

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        PictureInfo pic = (PictureInfo)getItem(position);
        View view = LayoutInflater.from(getContext()).inflate(id, parent, false);
        ImageView checkBox = view.findViewById(R.id.check_box_2);
        if (position >= firstVisible && position < firstVisible + visibleNum) {
            Bitmap bitmap = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                try {
                    bitmap = getContext().getContentResolver().loadThumbnail(pic.getUri(), new Size(200, 200), null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            ImageView imageView = view.findViewById(R.id.piece_picture_view);
            imageView.setImageBitmap(bitmap);
            checkBox.setImageBitmap(TransferFilesActivityPresenter.getUncheckedIcon());
        }
        if (selectedIndex.contains(position)) {
            checkBox.setImageBitmap(TransferFilesActivityPresenter.getCheckedIcon());
        }
        return view;
    }


    public PictureListAdapter(@NonNull Context context, int resource, ArrayList<PictureInfo> pictures) {
        super(context, resource, pictures);
    }
}
