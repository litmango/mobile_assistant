package com.kylin.mobileassistant.presenter.transferfiles;

import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.adapter.MusicListAdapter;
import com.kylin.mobileassistant.base.presenter.BasePresenter;
import com.kylin.mobileassistant.data.MusicInfo;
import com.kylin.mobileassistant.ui.activity.interfaces.transferfiles.IMusicsFragment;
import com.kylin.mobileassistant.ui.activity.transferfiles.TransferFilesActivity;

import java.util.ArrayList;

public class MusicsFragmentPresenter extends BasePresenter<IMusicsFragment> {
    private final ArrayList<MusicInfo> musics = new ArrayList<>();
    private MusicListAdapter musicListAdapter = null;

    public void loadMusicView(View view, TransferFilesActivity transferFilesActivity) {
        getMusicsInfo(view);
        musicListAdapter = new MusicListAdapter(view.getContext(), R.layout.piece_music, musics);
        ListView listView = view.findViewById(R.id.musics_view);
        listView.setAdapter(musicListAdapter);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if(scrollState == SCROLL_STATE_IDLE) {
                    musicListAdapter.notifyDataSetChanged();
                    transferFilesActivity.setViewPagerSlide(true);
                } else {
                    transferFilesActivity.setViewPagerSlide(false);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                musicListAdapter.setFirstVisible(firstVisibleItem);
                musicListAdapter.setVisibleNum(visibleItemCount);
            }
        });
        listView.setOnItemClickListener((parent, view1, position, id) -> {
            View child = listView.getChildAt(position - musicListAdapter.getFirstVisible());
            ImageView imageView = child.findViewById(R.id.check_box_5);
            if(musicListAdapter.isContained(position)) {
                musicListAdapter.unSelecting(position);
                imageView.setImageBitmap(TransferFilesActivityPresenter.getUncheckedIcon());
            } else {
                musicListAdapter.selecting(position);
                imageView.setImageBitmap(TransferFilesActivityPresenter.getCheckedIcon());
            }
            TextView selectedNumView = transferFilesActivity.findViewById(R.id.text_select);
            String text = "已选择" + musicListAdapter.size() + "项";
            selectedNumView.setText(text);
        });
    }

    private void getMusicsInfo(View view) {
        String[] projection = new String[]{
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.SIZE,
                MediaStore.Audio.Media.DURATION
        };
        Cursor cursor = view.getContext().getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection, null,
                null, MediaStore.Audio.Media.DATE_MODIFIED + " DESC"
        );
        int nameIndex = cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME);
        int pathIndex = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);
        int durationIndex = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            durationIndex = cursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
        }
        int sizeIndex = cursor.getColumnIndex(MediaStore.Audio.Media.SIZE);
        for(cursor.moveToFirst();cursor.moveToNext();) {
            String path = cursor.getString(pathIndex);
            String name = cursor.getString(nameIndex);
            long duration;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                duration = cursor.getLong(durationIndex);
            } else {
                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(path);
                duration = Integer.parseInt(mmr.extractMetadata
                        (MediaMetadataRetriever.METADATA_KEY_DURATION));

            }
            long size = cursor.getLong(sizeIndex);
            if(duration > 0) {
                musics.add(new MusicInfo(path, name, duration, size));
            }
        }
        cursor.close();
    }

    public void updateView(TransferFilesActivity transferFilesActivity) {
        TextView selectedNumView = transferFilesActivity.findViewById(R.id.text_select);
        String text = "已选择" + musicListAdapter.size() + "项";
        selectedNumView.setText(text);
        musicListAdapter.notifyDataSetChanged();
    }
}
