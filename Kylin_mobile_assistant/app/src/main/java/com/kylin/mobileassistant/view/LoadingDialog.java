package com.kylin.mobileassistant.view;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.TextView;

import com.kylin.mobileassistant.R;

/**
 * @author : zhaoyang
 * @date : 2019/10/24
 */
public class LoadingDialog extends Dialog {
    private String content;
    private boolean cancelable;

    public LoadingDialog(Context context, String content, boolean cancelable) {
        super(context, R.style.base_LoadingDialog);
        this.content = content;
        this.cancelable = cancelable;
        initView();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && cancelable) {
            if (isShowing()) {
                dismiss();
            }
        }
        return false;
    }

    private void initView() {
        setContentView(R.layout.base_view_loading_dialog);
        ((TextView) findViewById(R.id.id_view_loading_dialog_tv_content)).setText(content);
        setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.alpha = 0.8f;
        getWindow().setAttributes(attributes);
        setCancelable(false);
    }
}
