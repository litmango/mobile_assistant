package com.kylin.mobileassistant.ui.activity.transferfiles;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.base.view.BaseMvpFragment;
import com.kylin.mobileassistant.presenter.transferfiles.ApplicationsFragmentPresenter;
import com.kylin.mobileassistant.ui.activity.interfaces.transferfiles.IApplicationsFragment;

public class ApplicationsFragment extends BaseMvpFragment <ApplicationsFragmentPresenter>
        implements IApplicationsFragment {
    TransferFilesActivity parent;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.loadApplicationView(view, parent);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        parent = (TransferFilesActivity)context;
    }

    @Override
    public void onResume() {
        mPresenter.updateView(parent);
        super.onResume();
    }

    @Override
    protected void initPresenter() {
        mPresenter = new ApplicationsFragmentPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_send_applications;
    }
}
