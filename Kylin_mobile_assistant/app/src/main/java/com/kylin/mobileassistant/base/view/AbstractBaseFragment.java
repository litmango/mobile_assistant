package com.kylin.mobileassistant.base.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.ColorRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * fragment的抽象基类
 */
public abstract class AbstractBaseFragment extends Fragment {

    private Unbinder mUnbinder;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getLifecycle().addObserver(DialogManager.get());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflateView(inflater, container);
        // view注入
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    protected View inflateView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    /**
     *
     * @return statusbar的高度
     */
    protected int getStatusBarHeight() {
        int result = 0;
        //获取状态栏高度的资源id
        int resourceId = getActivity().getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getActivity().getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    protected void initStatusBar() {
    }

    /**
     * @return fragment所依附的activity
     */
    public AbstractBaseActivity getBaseActivity() {
        if (isAdded()) {
            return (AbstractBaseActivity) getActivity();
        }
        return null;
    }

    /**
     * 返回fragment的tag
     *
     * @return fragment的tag
     */
    public String getFragmentTag() {
        return this.getClass().getSimpleName();
    }

    /**
     * 页面布局id
     *
     * @return id
     */
    @LayoutRes
    protected abstract int getLayoutId();

    protected int getColor(@ColorRes int resId) {
        return ResourcesCompat.getColor(getResources(), resId, null);
    }

    /**
     * activity是否可见
     *
     * @return true 可见，false 不可见
     */
    protected boolean isActivityVisible() {
        return this.isAdded() && getActivity() != null && getActivity() instanceof AbstractBaseActivity && ((AbstractBaseActivity) getActivity()).isVisible();
    }

    /**
     * fragment是否可见
     *
     * @return true 可见，false 不可见
     */
    public boolean isFragmentVisible() {
        return isActivityVisible() && isVisible();
    }

    /**
     * 跳转系统的App应用详情页
     */
    protected void toAppDetailSetting(Context context) {
        Intent localIntent = new Intent();
        localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        localIntent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        localIntent.setData(Uri.fromParts("package", context.getPackageName(), null));
        startActivity(localIntent);
    }

    /**
     * 权限请求结果监听者
     */
    public interface OnPermissionResultListener {
        /**
         * 权限被允许
         */
        void onAllow();

        /**
         * 权限被拒绝
         */
        void onReject();
    }
}
