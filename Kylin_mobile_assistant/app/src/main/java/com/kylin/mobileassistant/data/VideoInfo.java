package com.kylin.mobileassistant.data;

import android.net.Uri;

import com.kylin.mobileassistant.utils.TransformUtil;

public class VideoInfo extends BaseInfo {
    private final Uri thumbUri;
    private final long duration;

    public VideoInfo(String path, String name, Uri uri, long duration, long size) {
        super(path, name, size);
        this.thumbUri = uri;
        this.duration = duration;
    }

    public Uri getThumbUri() {
        return thumbUri;
    }

    public String getDuration() {
        return TransformUtil.turnSecondsToHours(duration / 1000);
    }
}
