package com.kylin.mobileassistant.base.view;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.Nullable;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.base.interfaces.IBaseContract;
import com.kylin.mobileassistant.base.presenter.BasePresenter;
import com.kylin.mobileassistant.base.presenter.PresenterManager;
import com.kylin.mobileassistant.utils.ToastUtil;
import com.kylin.mobileassistant.view.LoadingDialog;

/**
 * @author zhaoyang
 * @date 2020/7/30
 * MVP 架构的activity的基类，如果不使用databinding继承此类，否则继承BaseMvpBindingActivity)
 */
public abstract class BaseMvpActivity<T extends BasePresenter> extends AbstractBaseActivity
        implements IBaseContract.IBaseMvpView {

    protected T mPresenter;

    protected PresenterManager mPresenterManager;

    private LoadingDialog mLoadingDialog;

    /**
     * Create new instance of fragment, restore instance of presenter from cache if needed.
     *
     * @param savedInstanceState saved instance of fragment
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPresenter();
        mPresenterManager = new PresenterManager();

        if (savedInstanceState != null && mPresenterManager.containsPresenter(savedInstanceState)) {
            mPresenter = mPresenterManager.<T>restorePresenter(savedInstanceState);
        }

        if (mPresenter != null) {
            mPresenter.attachView(this);
        }
    }

    /**
     * 初始化presenter
     */
    protected abstract void initPresenter();

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mPresenter != null) {
            mPresenter.onViewStart();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mPresenter != null) {
            mPresenter.onViewStop();
        }
    }

    /**
     * Save presenter using presenter manager.
     *
     * @param outState out state of saved instance
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        try {
            super.onSaveInstanceState(outState);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mPresenter != null) {
            mPresenterManager.savePresenter(mPresenter, outState);
        }
    }

    /**
     * Detach view from presenter.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mPresenter != null) {
            mPresenter.detachView();
        }
    }

    /**
     * Returns instance of presenter.
     *
     * @return instance of presenter
     */
    public T getPresenter() {
        return mPresenter;
    }

    @Override
    public void showLoading() {
        if (mLoadingDialog != null) {
            mLoadingDialog.dismiss();
        }
        mLoadingDialog = new LoadingDialog(this, "加载中", true);
        mLoadingDialog.show();
    }

    @Override
    public void showLoading(String content, boolean cancelable) {
        try {
            if (mLoadingDialog != null) {
                mLoadingDialog.dismiss();
            }
            mLoadingDialog = new LoadingDialog(this, content, cancelable);
            mLoadingDialog.show();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showLoading(boolean cancelable) {
        if (mLoadingDialog != null) {
            mLoadingDialog.dismiss();
        }
        mLoadingDialog = new LoadingDialog(this, "加载中", cancelable);
        mLoadingDialog.show();
    }

    @Override
    public void showLoading(String content) {
        try {
            if (mLoadingDialog != null) {
                mLoadingDialog.dismiss();
            }
            mLoadingDialog = new LoadingDialog(this, content, true);
            mLoadingDialog.show();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showBusinessToast(String content, int type) {
        ToastUtil.showBottom(this, content, type);
    }

    @Override
    public void hideInput() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        View v = getWindow().peekDecorView();
        if (null != v) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    @Override
    public void showInput(View et) {
        et.requestFocus();
        InputMethodManager imm = (InputMethodManager)
                getSystemService(INPUT_METHOD_SERVICE);
        imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void showAppToast(String content, int type) {
        ToastUtil.showBottom(this.getApplication(), content, type);
    }

    @Override
    public void stopLoading() {
        if (mLoadingDialog != null) {
            mLoadingDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        stopLoading();
        super.onBackPressed();
    }

}
