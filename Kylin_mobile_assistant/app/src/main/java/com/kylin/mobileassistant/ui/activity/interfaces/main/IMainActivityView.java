package com.kylin.mobileassistant.ui.activity.interfaces.main;

import com.kylin.mobileassistant.base.interfaces.IBaseContract;

/**
 * @author : mechrevo
 * @date : 2021/12/10 15:58
 */
public interface IMainActivityView extends IBaseContract.IBaseMvpView {
}
